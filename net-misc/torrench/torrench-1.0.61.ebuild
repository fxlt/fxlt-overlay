# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python3_{4..6} )

inherit distutils-r1

DESCRIPTION="Command-line torrent search program (cross-platform)."
HOMEPAGE="https://kryptxy.github.io/torrench/"
SRC_URI="https://github.com/kryptxy/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]"
RDEPEND="
    ${DEPEND}
    dev-python/beautifulsoup:4[${PYTHON_USEDEP}]
    dev-python/colorama[${PYTHON_USEDEP}]
    dev-python/lxml[${PYTHON_USEDEP}]
    dev-python/pyperclip[${PYTHON_USEDEP}]
    dev-python/requests[${PYTHON_USEDEP}]
    dev-python/tabulate[${PYTHON_USEDEP}]
"

python_install() {
    insinto /usr/share/${PN}/
    newins torrench.ini torrench.ini.example
    distutils-r1_python_install --optimize=1
}

pkg_postinst() {
    elog "Fetching torrents from TPB, KAT, ST, Nyaa, XBit, etc is disabled by default."
    elog "The user must configure and enable it to use. Please read the docs for "
    elog "configuration instructions."
    elog ""
    elog "https://github.com/kryptxy/torrench"
    elog ""
    elog "Example torrench.ini config file installed into /usr/share/${PN}/torrench.ini.example"
}
