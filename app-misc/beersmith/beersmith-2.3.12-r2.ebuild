# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit desktop unpacker

MY_PN="BeerSmith"

DESCRIPTION="BeerSmith Home Brewing Software"
HOMEPAGE="http://www.beersmith.com/"
SRC_URI="https://s3.amazonaws.com/${PN}2-3/${MY_PN}-${PV}_17_04amd64.deb"

LICENSE="${MY_PN}
	GPL-2"

SLOT="0"
KEYWORDS="-* ~amd64"
IUSE=""

RESTRICT="mirror"

DEPEND=""
RDEPEND="media-libs/libpng:/16
    dev-libs/glib:2
    sys-libs/zlib
    x11-libs/cairo
    x11-libs/gdk-pixbuf:2
    x11-libs/gtk+:2
    x11-libs/libSM
    x11-libs/libX11
    x11-libs/pango"

S="${WORKDIR}"
QA_PREBUILT="opt/bin/beersmith2"

src_install() {
    into /opt
    dobin usr/bin/beersmith2

    insinto /usr/share/${MY_PN}2
    doins -r usr/share/${MY_PN}2/{*.bsmx,*.xml,*.bsopt,Reports,Updates,help}

    insinto /usr/share/${MY_PN}2/icons
    doins usr/share/${MY_PN}2/icons/*.{gif,png}

    sed -i \
        -e 's#/usr/bin/##' \
        -e '/^Categories/ s/$/;/' \
        usr/share/applications/beersmith2.desktop
    domenu usr/share/applications/beersmith2.desktop

    for size in 16 24 32 48 64 128 ; do
        insinto /usr/share/icons/hicolor/${size}x${size}/apps
        doins usr/share/icons/hicolor/${size}x${size}/apps/${PN}.png
    done
}
