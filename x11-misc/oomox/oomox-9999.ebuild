# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit git-r3

DESCRIPTION="Graphical application for generating different color variations of a Numix-based and Materia themes (GTK2, GTK3), Gnome-Colors and Archdroid icons"
HOMEPAGE="https://github.com/actionless/oomox"
SRC_URI=""
EGIT_REPO_URI="https://github.com/actionless/oomox.git"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
    x11-libs/gdk-pixbuf
    dev-python/pygobject
    x11-themes/gtk-engines-murrine
    dev-lang/sassc
    gnome-base/librsvg
    media-gfx/imagemagick
    app-arch/zip
    media-gfx/optipng
    sys-process/parallel
    media-gfx/inkscape"
RDEPEND="${DEPEND}"

src_configure() {
    true
}

src_compile() {
    true
}

src_install() {
    mkdir "${D}/usr/lib" "${D}/usr/bin" -p
    cp -R "${S}/" "${D}/usr/lib/oomox" || die
    cp "${FILESDIR}/oomox-gui" "${D}/usr/bin/" || die
    cp "${FILESDIR}/oomox-cli" "${D}/usr/bin/" || die
}