# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit autotools

DESCRIPTION="A backup program for disk arrays."
HOMEPAGE="http://www.snapraid.it/"
SRC_URI="https://github.com/amadvance/${PN}/releases/download/v${PV}/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

DOCS=( "AUTHORS" "HISTORY" "README" "TODO" "snapraid.conf.example" )

# PATCHES=( "${FILESDIR}/${P}-minor.patch" )

src_prepare() {
	default
	eautoreconf
}