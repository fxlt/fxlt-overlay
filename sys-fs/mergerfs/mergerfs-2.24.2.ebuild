# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

# inherit git-2

DESCRIPTION="A featureful union filesystem"
HOMEPAGE="https://github.com/trapexit/mergerfs"
SRC_URI="https://github.com/trapexit/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="sys-apps/attr
        sys-devel/libtool
        sys-devel/gettext
        sys-fs/fuse"

src_install() {
	dobin mergerfs
	dodoc README.md
	doman man/mergerfs.1
}
