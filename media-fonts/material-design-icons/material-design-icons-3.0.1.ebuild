# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6
inherit font

DESCRIPTION="Material design icons by Google"
HOMEPAGE="https://github.com/google/material-design-icons"
SRC_URI="https://github.com/google/material-design-icons/releases/download/${PV}/material-design-icons-${PV}.zip -> ${P}.zip"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

FONT_SUFFIX="ttf"
FONT_S="${WORKDIR}/material-design-icons-${PV}/iconfont"
S=${FONT_S}
