# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit font

MY_PN="${PN}-${MY_PV}"

DESCRIPTION="215 weather themed icons"
HOMEPAGE="http://weathericons.io"
SRC_URI="https://github.com/erikflowers/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

FONT_SUFFIX="ttf"
FONT_S="${WORKDIR}/${P}/font"
S=${FONT_S}