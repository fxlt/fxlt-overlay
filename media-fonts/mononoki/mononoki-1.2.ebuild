# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit font

Description="A font for programming and code review"
HOMEPAGE="https://madmalik.github.io/mononoki/"
SRC_URI="https://github.com/madmalik/${PN}/releases/download/${PV}/${PN}.zip -> ${P}.zip"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

FONT_SUFFIX="ttf"
FONT_S="${WORKDIR}"
S="${FONT_S}"